import org.junit.jupiter.api.{Assertions, Test}

class TradeTest {
  @Test
  def valueReturnsCorrectly_test(): Unit ={
    // given
    val trade = EquityTrade("T1", "APPL", 344, 3)
    val expected = 1032
    Assertions.assertEquals(expected, trade.value)
  }
}
