
abstract class Trade (id: String, private var _price: Double):
  require(_price > 0)
  def price: Double = _price

  def price_=(newPrice: Double): Unit =
    if (newPrice <= 0)
      throw new IllegalArgumentException("Price cannot be negative")

    this._price = newPrice

  def isExecutable: Boolean
end Trade

class EquityTrade(id: String, symbol: String, quantity: Int, private var _price: Double) extends Trade(id, _price):
  override def isExecutable: Boolean = true
  def value: Double = _price * quantity

end EquityTrade

class FXTrade(id: String, private var _price: Double) extends Trade(id, _price):
  override def isExecutable: Boolean = false
end FXTrade



@main def main(): Unit = {
  println("Hello OOP")
  val t1 = EquityTrade("T1", "APPL", 100, 34)
  t1.price = 3434
  println(t1)
  println(t1.price)
}


