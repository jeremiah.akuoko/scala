import java.time.LocalDate
import java.time.format.{DateTimeFormatter, FormatStyle}
import java.util.Date
import scala.io.Source;

abstract class Person() {
  def sayHello(): Unit = {
    println("Hello Jerry")
  }
}

@main def run(): Unit = {
  println("Hello Chairman")
  val celsius = 53.4
  println(s"$celsius degC is ${celsiusFahrenheitConverter(celsius)} F")
  println(dateConverter("01/02/15"))
  extractFieldsFromPasswd("passwd")
}

def celsiusFahrenheitConverter(celsius: Double): Double = {
  (celsius * 9 / 5) + 32
}

def dateConverter(dateString: String): String = {
  val datePattern = "^([0-9]{2})/([0-9]{2})/([0-9]{2})$".r
  dateString match
    case datePattern(day, month, year) => {
      val date = LocalDate.of(2000 + year.toInt, month.toInt, day.toInt)
      date.format(DateTimeFormatter.ofLocalizedDate(FormatStyle.FULL))
    }
}

def extractFieldsFromPasswd(fileName: String): Unit = {
  val bufferedSource = Source.fromResource(fileName);
  val passwdRegex = "(.*):(.*):(.*):(.*):(.*):(.*):(.*)".r
  for (line <- bufferedSource.getLines) {
    line match
      case passwdRegex(username, password, userId, groupId, description, homeDirectory, shell) => {
        println(s"Username: $username, " +
          s"Password: $password, User ID: $userId, " +
          s"Group ID: $groupId, Description: $description, " +
          s"Home Directory: $homeDirectory, Shell: $shell")
      }
  }
  bufferedSource.close()
}