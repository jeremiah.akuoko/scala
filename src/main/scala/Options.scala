@main def mainOptional(): Unit =
  val countries = Map("GH" -> "Ghana", "UK" -> "United Kingdom")
  println(countries.getOrElse("GHS", null))

  try
    val integer = "d3".toInt
    println(s"Was able to convert, $integer")
  catch
    case e: NumberFormatException =>
      println("You tried to convert an invalid string to number")
      println(e.getMessage)
