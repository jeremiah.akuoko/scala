


def composeFunctionFromString(operation: String): (Double, Double) => Double = {
  operation match
    case "add" => (x: Double, y: Double) => x + y
    case "subtract" => (x: Double, y: Double) => x - y
    case "power" => (x: Double, y: Double) => Math.pow(x, y)
    case "divide" => (x: Double, y: Double) => x / y
    case _ => throw IllegalArgumentException("Invalid operation")
}

@main def runner(): Unit = {
  val add = composeFunctionFromString("add")
  println(add(343.2, 434.2))
  val subtract = composeFunctionFromString("subtract")
  println(subtract(343.2, 434.2))
  val power = composeFunctionFromString("power")
  println(power(3, 3))
  val divide = composeFunctionFromString("divide")
  println(divide(343.2, 434.2))
}
