

enum MembershipType(val limit: Int):
  case Junior extends MembershipType(3)
  case Regular extends MembershipType(8)
  case Senior extends MembershipType(15)
end MembershipType


val meat = new Function[Int, String]{
  override def apply(v1: Int): String = v1.toString
}

case class Member(name: String, membershipType: MembershipType)

object LibraryLab  extends  App {
  println(meat(3434))
  val juniorMember =   Member("Jerry", MembershipType.Junior)
  val regularMember =   Member("Jerry Re", MembershipType.Regular)
  val seniorMember =   Member("Jerry Snr", MembershipType.Senior)

  def canBorrow(member: Member, numberOfBooks: Int): Boolean =
    member match
      case Member(_, membershipType) => numberOfBooks <= membershipType.limit

  println(canBorrow(juniorMember, 34))
  println(regularMember)
  println(seniorMember)
}

