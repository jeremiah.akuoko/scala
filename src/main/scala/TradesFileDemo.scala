object MyApp extends App {
  val source = io.Source.fromResource("Trades.txt")
  for (record <- source.getLines) {
    val fields = record.split(",").map(_.trim)
    println(s"${fields(0)}\t${fields(3)}\t${fields(4)}")
  }
  source.close
}

