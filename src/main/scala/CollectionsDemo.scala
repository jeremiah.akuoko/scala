import scala.annotation.tailrec
import scala.collection.mutable.{ArrayBuffer, Seq as MutableSeq}
import scala.collection.immutable.TreeSet
import scala.util.Random

case class Employee(name: String)

def remove[A](coll: Seq[A] | MutableSeq[A], idx: Int): Seq[A] | MutableSeq[A] =
  if (coll.isEmpty)
    throw UnsupportedOperationException("Can't perform remove on an empty Sequence")
  if (idx < 0 || idx > (coll.length - 1))
    throw IndexOutOfBoundsException(s"The index: $idx is out of bounds. Range (0-${coll.length - 1})")
  coll.slice(0, idx) concat coll.slice(idx + 1, coll.length)

@tailrec
def getRandomUniqueRange(numbers: Set[Int], limit: Int, from: Int, to: Int): Set[Int] =
  if(numbers.size == limit)
    numbers
  else {
    val randomInt = Random.between(from, to + 1)
    getRandomUniqueRange(numbers ++ Set(randomInt), limit, from, to)
  }


@main def mainRunner(): Unit =
  println(List(324) ++ List(324, 34) :+ 12)
  println(getRandomUniqueRange(TreeSet(), 6, 1, 49))
//  val mutableSeq = MutableSeq(34, 32, 63)
//  val employees = Seq(Employee("Jerry"),Employee("Jerry"), Employee("Jerry"))
//  val numbers = Seq(3, 42, 12, 35, 22, -2, 2, 4, -23, -234)
//  val newNumbers = remove(numbers, 9)
//  println(numbers)
//  println(newNumbers)
//
//  val newEmployees = remove(employees, 0)
//  println(employees)
//  println(newEmployees)
//
//  val mutableNumbers = MutableSeq(3, 42, 12, 35, 22, -2, 2, 4, -23, -234)
//  mutableNumbers.update(1, 3434)
//
//  println(mutableNumbers)
//  val newMutableNumbers = remove(mutableNumbers, 4)
//  println(newMutableNumbers)

