object Exercise2 extends App {
  val sumLengths: (String, String, String, String, String) => Int =
    _.length + _.length + _.length + _.length + _.length

  println(sumLengths("232", "3444", "9932", "3", ""))

  val fun2: (Int) => () => Double = (x: Int) => () => Math.pow(x.toDouble, 3)

  println(fun2(4)())
//
//  val function: (String, (Int, (String => Int)) => Int) => (Int => Int) =
//    (text1: String)

}
