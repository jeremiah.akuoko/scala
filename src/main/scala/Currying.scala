import java.io.File

object Currying {

  private val filesHere: Array[File] = new java.io.File(".").listFiles()

  /* Use currying, this allows for partial application and also nicer syntax */
  def fileMatching(query: String)(predicate: (String, String) => Boolean): Array[File] = {
    for (file <- filesHere if predicate(file.getName, query)) yield file
  }

  /* These functions are defined using partial application - they are more specialised verstions of the function above*/
  def filesEnding: String => Array[File] = fileMatching (_) (_ endsWith _)
  def filesContaining: String => Array[File] = fileMatching(_)(_ contains _)
  def filesRegex: String => Array[File] = fileMatching(_)(_ matches _)
  def filesStartsWith: String => Array[File] = fileMatching(_)(_ startsWith _)

  /* Adding the parameter that we haven't provided above, allows us to get a value (as opposed to a function) */
  val filesEndingGIT: Array[File] = filesEnding("git")
  val filesContainingSRC: Array[File] = filesContaining("src")
  val filesRegexLotsOfFiles: Array[File] = filesRegex("^[A-Za-z0-9./]*$")

  /* As curried, we can use curly braces for the second parameter, giving a more natural feeling syntax */
  def filesComplexLookup(query: String): Array[File] = fileMatching(query) {
    (fileName, queryString) => {
      fileName.length < 5 &&
        fileName.contains(queryString)
    }
  }

  def main(args: Array[String]): Unit = {

    println(filesEndingGIT.mkString("Array(", ", ", ")"))
    println(filesContainingSRC.mkString("Array(", ", ", ")"))
    println(filesRegexLotsOfFiles.mkString("Array(", ", ", ")"))

    println(filesStartsWith(".bs").mkString("Array(", ", ", ")"))
    println(filesComplexLookup("r").mkString("Array(", ", ", ")"))
  }
}
